#include <assert.h>
#include "scratch.h"

static int iarr_push (struct _scratch_iarr *iarr, int val) {
	if (iarr->n >= iarr->a) {
		iarr->a *= 2;
		int *tmp = realloc (iarr->d, sizeof *iarr->d * iarr->a);
		if (!tmp)
			return 1;
		iarr->d = tmp;
	}
	iarr->d[iarr->n++] = val;
	return 0;
}

static int iarr_pop (struct _scratch_iarr *iarr) {
	assert (iarr->d && iarr->n);
	iarr->n--;
	return 0;
}

static int iarr_create (struct _scratch_iarr *res) {
	res->d = malloc (sizeof (int) * 8);
	if (!res->d) return 1;
	res->a = 8;
	res->n = 0;
	return 0;
}

struct scratch scratch_create (size_t size) {
	struct scratch res = {
		.base = malloc (sizeof *res.base * size),
	};
	if (!res.base)
		return (struct scratch) {0};
	res.a = size;
	if (iarr_create (&res.starts))
		return (struct scratch) {0};
	res.current_n = 0;
	return res;
}

// Don't pass a zeroed scratch to this. Use scratch_create first.
char *scratch_get (struct scratch *scr, size_t n) {
	// This if means maybe best to always have a starts, after all.
	// So, starts->n will always be one, and starts->d[0] will always
	// be 0.
	size_t new_idx = scr->starts.n
		? scr->starts.d[scr->starts.n  - 1] + scr->current_n
		: 0;
	if (scr->a < new_idx + n) {
		fprintf (stderr, "scratch: out of memory: increase buffer\n\
scr->base: %p, scr->current_n: %zu, scr->a: %zu, incoming: %zu\n\
	starts: ", scr->base, scr->current_n, scr->a, n);
		if (!scr->starts.n)
			fprintf (stderr, "none\n");
		else {
			fprintf (stderr, "n: %zu, a: %zu: d: {",
					scr->starts.n, scr->starts.a);
			for (int i = 0; i < scr->starts.n; i++) {
				fprintf (stderr, "scr->starts.d[%d]: %d",
						i, scr->starts.d[i]);
				if (i != scr->starts.n - 1) {
					fprintf (stderr, ", ");
				}
			}
			fprintf (stderr, "}\n");

			exit (1);
		}
		/* scr->a = (new_idx + n) + SCRATCH_EXTRA_ALLOC; */
		/* char *tmp = realloc (scr->base, sizeof *scr->base * scr->a); */
		/* if (!tmp) */
		/* 	return 0; */
		/* scr->base = tmp; */
	}
	if (iarr_push (&scr->starts, new_idx))
		return 0;
	scr->current_n = n;
	return &scr->base[scr->starts.d[scr->starts.n - 1]];
};

char *scratch_unget  (struct scratch *scr) {
	if (!scr->starts.d || scr->starts.n < 1)
		return 0;
	// Just set this to 0? It doesn't matter that we've lost the n. You already
	// have the info.
	scr->current_n = 0;
	iarr_pop (&scr->starts);
	if (!scr->starts.n)
		return scr->base;
	else
		return &scr->base[scr->starts.d[scr->starts.n - 1]];
}

void scratch_free (struct scratch *scr) {
	free (scr->base);
	free (scr->starts.d);
	*scr = (struct scratch) {0};
}

bool scratch_all_ungot (struct scratch *scr) {
	return scr->starts.n;
}

// For debugging. Gets used if you #define SCRATCH_DEBUG 1 before you
// include the lib. See header.
void _scratch_print (struct scratch *scr) {
	fprintf (stderr, "\
	{\n\
		.base = %p,\n\
		.current_n = %zu,\n\
		.a = %zu,\n\
		.starts = {\n\
			.d = %p,\n\
			.n = %zu,\n\
			.a = %zu,\n\
		},\n\
	}\n",
	scr->base,
	scr->current_n,
	scr->a,
	scr->starts.d,
	scr->starts.n,
	scr->starts.a);
}
