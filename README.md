# Scratch

## Real explanation

I'll leave what I originally wrote, because, well, it's the first part
of the story. But you can ignore it, unless you enjoy laughing at
people.

This "lib" was an attempt at a "growable scratch buffer". It was a bit
array of chars and some pointers to it. You'd ask for some memory
(scratch\_get) and give it back (scratch\_pop).

But it doesn't work. The reason is this:


```C
char *buf_1 = scratch_get (&scratch, TSK_N_SCRATCH);
char *buf_2 = $scratch_get (&scratch, 0xff);
```

What happens when you write use buf\_1? It depends. If the first
scratch\_get caused a realloc, what buf\_1 points to is now freed
memory.

So it doesn't work. It can't be done in this naive way.

So what it does now is no reallocing. It simply allocates memory in
the buffer you make with scratch\_create, and if it goes over it
calls assert.

Perhaps I'll report back and tell you if this is at all useful.

## Important note

I should say, first, that I'm not sure if there's a point to this lib,
that you probably shouldn't use it.

## Failed explanation

This is a growable scratch buffer. It's a big array of chars and bunch
of pointers into it. When you call the badly-named scratch\_push, you
get a pointer to somewhere in this buffer, which may have been resized
to the requested memory can fit.

You use it like this:

```
struct scratch scratch = scratch_create (0xfff);

char *buf_1 = scratch\_push (&scratch, 1000);
char *buf_2 = scratch\_push (&scratch, 2000);

// Do stuff with the bufs.

scratch\_pop (&scratch);
scratch\_pop (&scratch);
```

## What's the point?

Really, it's for string stuff. A lot of the time I find myself
allocating space for strings on the stack or heap, then passing that
string to a function and maybe allocating more strings. This may well
be a sign of bad program design.

My method of keeping down allocations has been to use a global
"scratch buffer" -- an array of chars. This works pretty well. But
what happens when you're using the contents of that buffer in a
lower-down function?

My solution to that has been to use a scratch\_2 buffer. Obviously
this isn't the most elegant solution.

I thought to myself, surely there's a way of getting more scratch
space automatically?

Well, there is, of course. It's called (drumroll) the stack. And the
great thing about the stack is you don't have to clean up the memory.

The stack is fine. And to be honest, I think it's better than this
solution. It's 8192 bytes deep on my Linux machine, which is
definitely enough to so the kinds of string manipulation I'm imagining
using this library with.

Are there advantages of using this lib? Well, you'll be able to
work with more memory. Also, Wikipedia says something I skimmed about
how allocating on the stack can incur some performance penalties.

## Random notes

scratch\_push is badly named, since it's not pushing anything on to
the buf; it's just allocating more memory if necessary and giving you
a pointer.

## Debugging

The big problem with this lib is you need to remember to "free" the
memory. And unlike mallocs, Valgrind won't help you.

If you forget a scratch\_pop, (you should call scratch\_all\_popped at
the end of your program to find this out), write `#define
SCRATCH\_DEBUG 1` before where you include the file.

This will print a bu

I'm sure I can think of a better method. Some more automatic way of
notifying. Using a stack. But it eludes me now.

Note that current\_n means the "length of the current 'buffer'". It's
the number of bytes you've asked for. I set it to 0 in stack\_pop,
because I don't retain the size of all "buffers". I don't have the
vocabulary. But I mention this so you don't get confused. All that's
really relevant in the debug output is the stuff in starts. At the
end, .n should read 0.

## Todo

* Better stat stuff. SCRATCH\_DEBUG should also output the highest
  "stack depth", and the total amount of memory used.

* Perhaps a trim memory function. At the moment my thinking is you
  should just free it and start again if you need to.

* A scratch\_end function that checks you've popped properly would be
  better than the scratch\_free and scratch\_all\_popped I have now.

* A macro scope thing. I've seen people write defer macros that defer
  actions, like Go's defer keyword. Something like that might be
  helpful. Well, those macros are generic, aren't they? So maybe I
  should just say use that if you want.

* A static version of this lib. You'd say the buf size is 0xxxxx, and
  that'd be it, apart from debug checking. Though, why not, in that
  case, just declare a big array of chars?
