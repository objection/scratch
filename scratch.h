#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

// When .base is reallocated, enough space to fit the array is allocated, plus this
// val. Set it to whatever you like. No doubt there's a better value that this.
#if !defined SCRATCH_EXTRA_ALLOC
#define SCRATCH_EXTRA_ALLOC 0xfff
#endif

// I could make this void * or hide it from you in some other way, but it's
// making debuging hard and pissing me off.
struct _scratch_iarr {
	int *d;
	size_t n, a;
};

struct scratch {
	struct _scratch_iarr starts;
	size_t current_n;
	char *base;
	size_t a;
};
struct scratch scratch_create (size_t size);
char *scratch_get (struct scratch *scr, size_t n);
char *scratch_unget (struct scratch *scr);
void scratch_free (struct scratch *scr);
bool scratch_all_ungot (struct scratch *scr);
void _scratch_print (struct scratch *scr);

// This lib is pretty dangerous. Use this if you've forgetten a $scratch_pop.
// Something better would be useful, something involves tracking pushes and pops
// and aborting when you forget one. A simple stack should do it. I might do this.
#if SCRATCH_DEBUG

#define scratch_get(_scratch_buf, _val) \
	({ \
		char *res = scratch_get (_scratch_buf, _val); \
		fprintf (stderr, "scratch_get (%s):\n", __func__); \
		_scratch_print (_scratch_buf); \
		res; \
	})

#define scratch_unget(_scratch_buf) \
	({ \
		scratch_unget (_scratch_buf); \
		fprintf (stderr, "scratch_unget (%s):\n", __func__); \
		_scratch_print (_scratch_buf); \
	})

#endif
